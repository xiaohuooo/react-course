import { combineReducers } from "redux";

let dataState = [
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: 11,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 12,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 13,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 14,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 15,
    code: "ENG",
    name: "qwe",
  },
  {
    id: "",
    code: "",
    name: "",
  },
  {
    id: 17,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 18,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 19,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 20,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 21,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 22,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 23,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 24,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 25,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 26,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 27,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 28,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 29,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 30,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 31,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 32,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 33,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 34,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 35,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 36,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 37,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 38,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 39,
    code: "ENG",
    name: "qwe",
  },
  {
    id: 40,
    code: "ENG40",
    name: "qwe40",
  },
];
function data(preState = dataState, action) {
  let { type, data } = action;
  switch (type) {
    case "UPDATE_VALUE":
      const newState = [...preState];
      console.log(data.newValue, "--data.newValue");
      newState[data.index] = data.newValue;
      return newState;
    case "SWAP_VALUE":
      const newList = [...preState];
      const swap = newList[data.oldIndex];
      newList[data.oldIndex]=newList[data.newIndex]
      newList[data.newIndex]=swap
      return newList;
    default:
      return preState;
  }
}
let eventState = {};
function event(preState = eventState, action) {
  let { type, data } = action;
  switch (type) {
    case "item":
      const newData = { ...data };
      return newData;
    default:
      return preState;
  }
}
export default combineReducers({
  data,
  event,
});
