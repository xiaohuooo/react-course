import "./App.css";
import Router from './router'
function App() {
  return (
    <div className="wrap">
      <div className="nav noPrint">Undergraduate Course Mapper</div>
      <div className="main">
        <Router />
      </div>
      <div className="footer noPrint"></div>
    </div>
  );
}

export default App;
