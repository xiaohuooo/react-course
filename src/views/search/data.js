export const data = [
  {
    code: "ENG1001",
    "credit points": "",
    name: "Civil Engineering",
    details: "",
    overview: "",
    offerings: [
      {
        "51-81-Clayton-On-Campus": {
          location: "Clayton",
          teaching_period: "First semester",
          "Attendance mode": "On-campus",
        },
      },
      {
        "52-81-Clayton-On-Campus": {
          location: "Clayton",
          "teaching period": "Second semester",
          "Attendance mode": "On-campus",
        },
      },
    ],
    prerequisiste: {
      OR: ["ENG1814", "ENG1060"],
      AND: ["ENG1005"],
      prohibiton: [],
    },
  },
  {
    code: "ENG1814",
    "credit points": "",
    name: "Electrical citcuits",
    details: "",
    overview: "",
    offerings: [
      {
        "51-81-Clayton-On-Campus": {
          location: "Clayton",
          teaching_period: "First semester",
          "Attendance mode": "On-campus",
        },
      },
      {
        "52-81-Clayton-On-Campus": {
          location: "Clayton",
          "teaching period": "Second semester",
          "Attendance mode": "On-campus",
        },
      },
    ],
    prerequisiste: {
      OR: ["ENG1814", "ENG1060"],
      AND: ["ENG1005"],
      prohibiton: [],
    },
  },
  {
    code: "ENG1005",
    "credit points": "",
    name: "Signals and systems",
    details: "",
    overview: "",
    offerings: [
      {
        "51-81-Clayton-On-Campus": {
          location: "Clayton",
          teaching_period: "First semester",
          "Attendance mode": "On-campus",
        },
      },
      {
        "52-81-Clayton-On-Campus": {
          location: "Clayton",
          "teaching period": "Second semester",
          "Attendance mode": "On-campus",
        },
      },
    ],
    prerequisiste: {
      OR: ["ENG1814", "ENG1060"],
      AND: ["ENG1005"],
      prohibiton: [],
    },
  },
  {
    code: "TRC4800",
    "credit points": "",
    name: "Robotics",
    details: "",
    overview: "",
    offerings: [
      {
        "51-81-Clayton-On-Campus": {
          location: "Clayton",
          teaching_period: "First semester",
          "Attendance mode": "On-campus",
        },
      },
      {
        "52-81-Clayton-On-Campus": {
          location: "Clayton",
          "teaching period": "Second semester",
          "Attendance mode": "On-campus",
        },
      },
    ],
    prerequisiste: {
      OR: ["ENG1814", "ENG1060"],
      AND: ["ENG1005"],
      prohibiton: [],
    },
  },
  {
    code: "FIT3171",
    "credit points": "",
    name: "Databases",
    details: "",
    overview: "",
    offerings: [
      {
        "51-81-Clayton-On-Campus": {
          location: "Clayton",
          teaching_period: "First semester",
          "Attendance mode": "On-campus",
        },
      },
      {
        "52-81-Clayton-On-Campus": {
          location: "Clayton",
          "teaching period": "Second semester",
          "Attendance mode": "On-campus",
        },
      },
    ],
    prerequisiste: {
      OR: ["ENG1814", "ENG1060"],
      AND: ["ENG1005"],
      prohibiton: [],
    },
  },
  {
    code: "ENG4701",
    "credit points": "",
    name: "FYP",
    details: "",
    overview: "",
    offerings: [
      {
        "51-81-Clayton-On-Campus": {
          location: "Clayton",
          teaching_period: "First semester",
          "Attendance mode": "On-campus",
        },
      },
      {
        "52-81-Clayton-On-Campus": {
          location: "Clayton",
          "teaching period": "Second semester",
          "Attendance mode": "On-campus",
        },
      },
    ],
    prerequisiste: {
      OR: ["ENG1814", "ENG1060"],
      AND: ["ENG1005"],
      prohibiton: [],
    },
  },
];
