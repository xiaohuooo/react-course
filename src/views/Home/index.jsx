import Card from "../../components/Card";
import { useEffect, useState, useRef } from "react";
import { Modal, Button } from "antd";
import { PlusOutlined, ExclamationCircleOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import { updateValue, swapValue } from "../../store/actions";
import CodeMirror from "@uiw/react-codemirror";
import { langs } from "@uiw/codemirror-extensions-langs";
import Sortable, { Swap } from "sortablejs";
import "./index.css";
import "./print.css"
Sortable.mount(new Swap());
const yearSem = ["2023", "2024", "2025", "2026", "2027"];
const Home = ({ updateValue, data, event, swapValue }) => {
  const history = useNavigate();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [item, setItem] = useState({});
  const cardFRef = useRef(null);
  const main = useRef(null);
  const showModal = (item) => {
    setIsModalOpen(true);
    setItem(item);
    console.log(item, "--item");
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const showConfirm = (e) => {
    e.stopPropagation(); //阻止原生事件冒泡
    Modal.confirm({
      title: "Do you Want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      content: "Some descriptions",
      onOk() {
        console.log("OK");
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };
  const checkDataOr = (data, event) => {
    for (let i = 0; i < event.prerequisiste.OR.length; i++) {
      const code = event.prerequisiste.OR[i];
      for (let j = 0; j < data.length; j++) {
        if (data[j].code === code) {
          return true;
        }
      }
    }
    return false;
  };
  const checkDataAND = (data, event) => {
    let andArr = event.prerequisiste.AND;
    let code = [];
    for (let j = 0; j < data.length; j++) {
      code.push(data[j].code);
    }
    for (let i = 0; i < andArr.length; i++) {
      if (!code.includes(andArr[i])) {
        return false;
      }
    }
    return true;
  };
  //保存数据
  const handleData = () => {
    // const datas = new Blob([JSON.stringify(data)], { type: 'text/plain' });
    // const file = new File([datas], 'data.js', {type: 'text/plain'});
    // const url = URL.createObjectURL(file);
    // const link = document.createElement('a');
    // link.href = url;
    // link.download = file.name;
    // document.body.appendChild(link);
    // link.click();
    // Modal.confirm({
    //   icon: "",
    //   content: (
    //     <CodeMirror
    //       value={JSON.stringify(data, null, 2)}
    //       height="auto"
    //       extensions={[langs.jsx()]}
    //       basicSetup={{
    //         foldGutter: true,
    //       }}
    //     />
    //   ),
    // });
    window.print();
  };
  const courseCheck = (events, id) => {
    if (!events.prerequisiste) return false;
    if (JSON.stringify(events) !== "{}") {
      // 计算上一组数据在数组中的起始位置和结束位置
      const end = Math.ceil((id * 1 + 1) / 4 - 1) * 4;
      const start = 0;
      if (id > 3) {
        let res = checkDataOr(data.slice(start, end), events);
        let res1 = checkDataAND(data.slice(start, end), events);
        return !(res && res1);
      }
    }
  };
  useEffect(() => {
    updateValue(event.id - 1, event);
    const sortable = Sortable.create(cardFRef.current, {
      animation: 150,
      easing: "cubic-bezier(1, 0, 0, 1)",
      swap: true, // Enable swap plugin
      swapClass: "highlight", // The class applied to the hovered swap item
      group: "shared",
      // draggable: ".draggable",
      onSort: (evt) => {
        const { oldIndex, newIndex } = evt;
        setTimeout(() => {
          swapValue(oldIndex, newIndex);
        }, 200);
      },
    });

    return () => sortable.destroy();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="home">
      <div className="hometop noPrint">
        Specialisalion - Mechalronics Engineering
      </div>
      <div className="homemain" ref={main}>
        <div className="homemain-left">
          {yearSem.map((item, index) => {
            return (
              <div className="year-container" key={item + new Date()}>
                <div className="year-text">{item}</div>
                <div className="sem-container">
                  <div>Samesler 1</div>
                  <div className="bottom">Samesler 2</div>
                  {index !== yearSem.length - 1 ? (
                    <span className="underline"></span>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            );
          })}
        </div>
        <div className="homemain-right">
          <div ref={cardFRef} className="cardF">
            {data.map((item, index) => {
              return item.id ? (
                <div
                  key={index + new Date()}
                  onClick={() => showModal(item)}
                  className={`${courseCheck(item, index) ? "error" : ""} ${
                    Math.floor(index / 4) % 2 !== 0 ? "bottom" : ""
                  }`}
                >
                  {courseCheck(item, index) ? (
                    <ExclamationCircleOutlined onClick={showConfirm} />
                  ) : (
                    ""
                  )}
                  <div style={{ fontSize: "16px" }}>{item.code}</div>
                  <div>{item.name}</div>
                </div>
              ) : (
                <div
                  className={`${
                    Math.floor(index / 4) % 2 !== 0 ? "bottom" : ""
                  }`}
                  key={index + new Date()}
                  onClick={() => history(`/search/${index}`)}
                >
                  <PlusOutlined></PlusOutlined>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className="homebottom noPrint">
        <div>
          <Button type="primary" ghost>
            Back
          </Button>
        </div>
        <div>
          <Button type="primary" onClick={handleData}>
            Submit
          </Button>
        </div>
      </div>
      <Modal
        title={item.code}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[]}
        width={800}
      >
        <Card title={"Advanced engineering mathematics"} item={item}></Card>
      </Modal>
    </div>
  );
};

export default connect(
  (state) => ({ data: state.data, event: state.event }), //映射状态
  { updateValue, swapValue } //映射操作状态的方法
)(Home);
